Pillow==6.0.0
beautifulsoup4==4.7.1
requests==2.22.0
tensorflow==2.0.0-beta1
numpy==1.16.2

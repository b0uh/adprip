

/*
 * 
 * faire npm -i puppeteer (je ne l'ajoute pas au package.json car ça télécharge chromium)
 * 
 * usage:
 * 
 * $ node embed.png.js
 * 
*/

const puppeteer = require('puppeteer')

const myUrl = 'https://www.adprip.fr/embed.html',
    cWidth = 550,
    cHeight = 90,
    imgOut = 'embed.png'

function delay(ms) {
    return new Promise((r) => setTimeout(r, ms))
}

;(async () => {
    // Launch the browser
    const browser = await puppeteer.launch({
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
        ],
        defaultViewport: {
            width: cWidth,
            height: cHeight,
            isLandscape: cWidth > cHeight,
        },
    })

    // Open a new page
    const page = await browser.newPage()

    // Navigate to URL
    await page.goto(myUrl)

    // Wait for page to be bully loaded
    await delay(2000)

    // Take screenshot
    await page.screenshot({ path: imgOut })

    await browser.close()
})()

'use strict';
const isIos = /ip(hone)/i.test(navigator.userAgent);
const isIpad = /ip(ad)/i.test(navigator.userAgent);
const isAndroid = /android/i.test(navigator.userAgent);
const isMobile = isIos || isIpad || isAndroid

var url = new URL(window.location.href)

// Nombre total de soutiens requis (4.7 Million)
const requiredTotal = 4717396

// calcul théorique pour estimer les 10 % du corps électoral nécessaire
// soit ~ 4 717 396 personnes / 274 jours (du 13 juin 2019 au 13 mars 2020)
// moyenne journalière à tenir d'environ 17 217 soutiens...
const perDay_required = 17217;

// Date d'aujourd'hui
const today = moment()

// Nombre de jours d'estimation sont ajoute au chart
var estimationDays = 7

// Le nombre de jours sur lequel estimer est configurable par l'url
const estimationParam = parseInt(url.searchParams.get('estimation'))
if (estimationParam >=0 && estimationParam <= 100) {
    estimationDays = estimationParam
}


$.get('./archive/history.dat', function (result) {
    $.csv.toArrays(result, {
        "separator": ";"
    }, function (err, rawData) {
        // *** ne pas décommenter les lignes ci-dessous *** //
        // *** version en test pour l'instant pour tenir compte de la date de maj (merci;) *** //
        // data.push([Math.floor(Date.now() / 1000), result["total"]]);
        // $.get('./archive/latest.count.json?nocache', function(result) {

        var mappedData = rawData.map(function(item) {
            return [
                moment(parseInt(1000 * item[0])).format("YYYY-MM-DD"),
                parseInt(item[1])
                ]
        })
        var data = mappedData.filter(function(item, pos) {
            if (pos === mappedData.length - 1) {
                return true
            } else {
                // Keep only latest timestamp of each day
                return (mappedData[pos + 1][0] !== item[0])
            }
        })
        console.log(data)

        var config = generateConfig(data)
        if (err) {
            console.log(err)
        } else {
            am4core.ready(function() {
                drawGauge(config)
                drawChart(config)
                chartTendance(config)
                $("#mainloader").hide()
            })
        }
    })
})

function generateConfig(data) {
    const dateStart = moment("2019-06-13");
    const dateEnd = moment("2020-03-12");

    const days_done = today.diff(dateStart, 'days') + 1; // date de départ incluse
    const days_remaining = dateEnd.diff(today, 'days') + 1; // date de fin non incluse

    const lastUpdate  = data[data.length - 1][0]
    const latestCount = data[data.length - 1][1]

    var averagePerDay_sinceStart = latestCount / days_done;
    const numberOfDays_estimation = Math.round((requiredTotal - latestCount) / averagePerDay_sinceStart);
    const average_currentWeek = Math.round((latestCount - data[data.length - 8][1]) / 7)

    var dateEnd_estimation = today.clone().add(numberOfDays_estimation, "days");

    var updateCheck = (lastUpdate !== moment().format("YYYY-MM-DD"));

    var chartData = [];
    var cumul = 0,
        difference,
        minimumNeeded,
        lastUpdateIndex = 0

    for (var i = 0, lgi = days_done + estimationDays; i < lgi; i++) {
        var visits = 0
        cumul += perDay_required;
        const dateStr = moment('2019-06-13').add(i, 'days').format("YYYY-MM-DD")
        data.forEach(arr => {
            if (dateStr === arr[0]) {
                visits = arr[1]
                difference = visits - cumul
                minimumNeeded = cumul
                lastUpdateIndex = i
            }
        })
        chartData.push({
            "date": dateStr,
            "cumul_theorique": cumul,
            "difference": difference
        })

        if (i > 0 && visits > 0) {
            chartData[i]['visits'] = visits
            chartData[i]['evolution'] = chartData[i]["visits"] - chartData[i - 1]["visits"]
        }
    }

    var dataSerie_minimum = chartData.filter(obj => obj["cumul_theorique"]);
    var dataSerie_visits = chartData.filter(obj => obj["visits"] && obj["visits"] !== 0);
    var dataSerie_officiel = [{
        date: "2019-07-01",
        visits: 465900
    }];

    var estimation = latestCount
    chartData.forEach(function(item, index) {
        if (index > 10 && !item.hasOwnProperty('visits')) {
            estimation += average_currentWeek
            item['estimation'] = estimation
        }
    })

    averagePerDay_sinceStart = (averagePerDay_sinceStart >= perDay_required) ?
        '<span>' + Math.round(averagePerDay_sinceStart).toLocaleString('fr') + '</span>' :
        '<span style="color: red" class="animated flash infinite">' + Math.round(averagePerDay_sinceStart).toLocaleString('fr') + '</span>';


    var evolutionText = (average_currentWeek > perDay_required) ?
        '<span style="color: green">&#8679;</span>' :
        (average_currentWeek === perDay_required) ?
        '<span style="color: orange">&#8678;&#8680;</span>' :
        '<span style="color: red">&#8681;</span>';

    var evolution_semaine = chartData.slice(lastUpdateIndex - 6, lastUpdateIndex + 1)
        .map(obj => '<div class="text-center">' + moment(obj["date"]).format("DD/MM/YYYY") + '&emsp;:&emsp;<b>' + obj["evolution"].toLocaleString('fr') + '</b></div>');

    var lastUpdate_msg = (moment(lastUpdate).isValid()) ?
        'dernière mise à jour le ' + moment(lastUpdate).format("DD/MM/YYYY") :
        'date de mise à jour non définie';

    if (updateCheck)
        evolution_semaine.splice(evolution_semaine.length - 1, 1, '<div class="text-center">' + moment().format("DD/MM/YYYY") +
            '&emsp;:&emsp;<b>en attente de mise à jour</b></div>');

    var contentStats = '<dl class="row">'
    contentStats += '<dt class="col-sm-3 text-right"><b>' + days_done + '</b></dt><dd class="col-sm-9">jours écoulés depuis le 13/06/2019</dd>'
    contentStats += '<dt class="col-sm-3 border-top"></dt><dd class="col-sm-9 border-top"></dd>'

    contentStats += '<dt class="col-sm-3 text-right"><b>' + latestCount.toLocaleString('fr') + '</b></dt><dd class="col-sm-9">soutiens publiés</dd>'
    contentStats += '<dt class="col-sm-3 text-right"><b>' + minimumNeeded.toLocaleString('fr') + '</b></dt><dd class="col-sm-9">minimum requis total</dd>'
    contentStats += '<dt class="col-sm-3 border-top"></dt><dd class="col-sm-9 border-top"></dd>'

    contentStats += '<dt class="col-sm-3 text-right"><b>' + perDay_required.toLocaleString('fr') + '</b></dt><dd class="col-sm-9">minimum de soutiens requis par jour</dd>'
    contentStats += '<dt class="col-sm-3 text-right"><b>' + averagePerDay_sinceStart + '</b></dt><dd class="col-sm-9">moyenne journali&egrave;re depuis le 13/06/2019</dd>'
    contentStats += '<dt class="col-sm-3 text-right"><b>' + average_currentWeek.toLocaleString('fr') + '</b></dt><dd class="col-sm-9">moyenne journali&egrave;re des 7 derniers jours</dd>'
    contentStats += '<dt class="col-sm-3 border-top"></dt><dd class="col-sm-9 border-top"></dd>'
    contentStats += '</dl>'

    contentStats += '<div>&Eacute;volution sur les 7 derniers jours</div>';
    contentStats += evolution_semaine.join('\n');

    $('#statsModalLabel').text('Statistiques au ' + moment(lastUpdate).format("DD/MM/YYYY"))
    $('#statsDiv').html(contentStats)
    document.getElementById('lastUpdate-span').innerHTML = ' - ' + lastUpdate_msg;

    var tendance_arrow = (average_currentWeek > perDay_required) ?
        '<span style="color: green">&#8679;</span>' :
        (average_currentWeek === perDay_required) ?
        '<span style="color: orange">&#8678;&#8680;</span>' :
        '<span style="color: red" title="tendance en baisse">&#8681;</span>';

    //  <option value="statistiques">Statistiques</option>
    var contentDropdown = (isMobile) ? '' :
        `<select id="selectCharts" class="form-control">
        <option value="evolution">Voir l'&eacute;volution</option>
        <option value="tendance">Voir la tendance</option>
        </select>`;

    var contentTendance = '<div id="soutiens_valides" class="d-inline-block"><span class="badge badge-secondary p-1 px-3" style="font-size: 14px">';
    contentTendance += latestCount.toLocaleString('fr') + '</span> soutiens publiés*&emsp;</div>';
    contentTendance += '<div id="perDay_required" class="d-inline-block"><span class="badge badge-secondary p-1 px-3" style="font-size: 14px">';
    contentTendance += minimumNeeded.toLocaleString('fr') + '</span> soutiens nécessaires&emsp;</div>';
    contentTendance += '<div id="average_currentWeek" class="d-inline-block"><span class="badge badge-secondary p-1 px-3" style="font-size: 14px">+ ';
    contentTendance += average_currentWeek.toLocaleString('fr') + '</span> moyenne par jour, 7 derniers jours&nbsp;' + tendance_arrow + '</div>';

    document.getElementById('tendance-dropdown').innerHTML = contentDropdown;
    document.getElementById('tendance-div').innerHTML = contentTendance;

    var updateDate = (moment(dateEnd_estimation).isSameOrBefore(dateEnd)) ?
        `serait atteint aux alentours du <b>` + dateEnd_estimation.format("DD/MM/YYYY") :
        `<span style="color:red">ne serait pas atteint pour le <b>` + dateEnd.format("DD/MM/YYYY") + `</span>`;

    var messagePC =
        `<i>Actuellement, le seuil des 4,7 millions d'électeurs soutenants la tenue d'un référendum ` +
        updateDate + `**</b>.</i>`;

    var messageMobile = `<i>Actuellement, le seuil des 4,7 millions d'électeurs ` + updateDate + `**</b>.</i>`;

    document.getElementById("output").innerHTML = isMobile ? messageMobile : messagePC;

    // only statistiques and gauge chart displayed on mobiles
    if (!isMobile)
        document.getElementById('selectCharts').onchange = (ev) => {
            if (ev.target.value === "evolution") {
                $('#chartTendance-div').hide();
                $('#chartdiv').fadeIn();
                $('#estimation-output').hide();
                $('#output').show();
            } else if (ev.target.value === "tendance") {
                $('#chartdiv').hide();
                $('#chartTendance-div').fadeIn();
                $('#output').hide();
                $('#estimation-output').show();
            } else {
                $('#chartTendance-div').hide();
                $('#chartdiv').hide();
                $('#estimation-output').hide();
                $('#output').show();
            }
        };

    // config amchart
    var menuOptions = [{
        "label": '<i class="fa fa-bars"></i>',
        "menu": [{
                "label": "Exporter les data",
                "menu": [{
                        "type": "json",
                        "label": "JSON"
                    },
                    {
                        "type": "csv",
                        "label": "CSV"
                    },
                    {
                        "type": "xlsx",
                        "label": "XLSX"
                    }
                ]
            },
            {
                "label": "Exporter une image",
                "menu": [{
                        "type": "png",
                        "label": "PNG"
                    },
                    {
                        "type": "jpg",
                        "label": "JPG"
                    },
                    {
                        "type": "svg",
                        "label": "SVG"
                    },
                ]
            }
        ]
    }];

    var config = {
        chartData: chartData,
        dateEnd: dateEnd,
        latestCount: latestCount,
        minimumNeeded: minimumNeeded,
        dataSerie_visits: dataSerie_visits,
        dataSerie_minimum: dataSerie_minimum,
        dataSerie_officiel: dataSerie_officiel,
        averagePerDay_sinceStart: averagePerDay_sinceStart,
        average_currentWeek: average_currentWeek,
        days_remaining: days_remaining,
        dateEnd_estimation: dateEnd_estimation,
        contentStats: contentStats,
        menuOptions: menuOptions,
        lastUpdate: moment(lastUpdate),
        lastUpdateIndex: lastUpdateIndex,
        updateCheck: updateCheck
    };

    return config;
}

function drawGauge(config) {
    var counter = config.latestCount,
        needed = config.minimumNeeded;

    // Themes begin
    am4core.useTheme(am4themes_material);

    // Icone d'avion
    var iconPath = "m2,106h28l24,22h72l-44,-53h35l80,52h98c21,0 21,21 0,21l-98,0 -80,64h-35l43,-63h-71l-24,20h-28l19,-32";

    var chart = am4core.create("gaugediv", am4charts.SlicedChart);
    chart.width = am4core.percent(95);
    chart.responsive.enabled = true;
    chart.tooltip.disabled = true

    // formatage
    chart.language.locale["_decimalSeparator"] = ",";
    chart.language.locale["_thousandSeparator"] = " ";

    var title = chart.titles.create();
    title.text = 'Au ' + config.lastUpdate.format('LL');
    title.fontSize = 18;
    title.fontWeight = '';
    title.marginBottom = 0;

    // Agrandi la surface coloree sur l'avion
    const amplify = 2

    var chartData
    if (needed > counter) {
        chartData = [{
            "name": "Soutiens publiés",
            "value": amplify * counter,
            "valueForLabel": counter,
            "forLabel": "soutiens publiés",
            "color": am4core.color("#67B7DC")
        }, {
            "name": "Retard",
            "value": amplify * (needed - counter),
            "valueForLabel": (needed - counter),
            "forLabel": "de retard",
            "color": am4core.color("#e59165")
        }, {
            "name": "",
            "value": requiredTotal - amplify * needed,
            "valueForLabel": requiredTotal - needed,
            "forLabel": "restant",
            "color": am4core.color("#dddddd")
        }]
    } else {
        chartData = [{
            "name": "Soutiens nécessaires",
            "value": amplify * needed,
            "valueForLabel": needed,
            "forLabel": "soutiens nécessaires",
            "color": am4core.color("#e59165")
        }, {
            "name": "Avance",
            "value": amplify * (counter - needed),
            "valueForLabel": counter,
            "forLabel": "soutiens publiés",
            "color": am4core.color("#67B7DC")
        }, {
            "name": "",
            "value": requiredTotal - amplify * counter,
            "valueForLabel": requiredTotal - counter,
            "forLabel": "restant",
            "color": am4core.color("#dddddd")
        }]
    }
    chart.data = chartData

    var series = chart.series.push(new am4charts.PictorialStackedSeries());
    series.tooltip.disabled = true
    series.dataFields.value = "value";
    series.dataFields.category = "name";
    series.maskSprite.path = iconPath;
    series.slices.template.propertyFields.fill = "color";
    series.slices.template.propertyFields.stroke = "color";
    series.labels.template.text = "[font-size: 1.1rem bold]{valueForLabel}[/] {forLabel}";
    series.labels.template.dy = 10
}

function drawChart(config) {
    am4core.useTheme(am4themes_material);
    am4core.options.autoSetClassName = true;

    var chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.data = config.chartData;

    // formatage
    chart.language.locale = am4lang_fr_FR;
    chart.language.locale["_date_day"] = "dd";
    chart.language.locale["_decimalSeparator"] = ",";
    chart.language.locale["_thousandSeparator"] = " ";
    chart.dateFormatter.dateFormat = "dd-MM-yyyy";

    // ajout du menu pour export divers
    chart.exporting.menu = new am4core.ExportMenu();
    chart.exporting.menu.items = config.menuOptions;

    // chart
    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.dateFormats.setKey("day", "dd/M");
    dateAxis.periodChangeDateFormats.setKey("day", "[bold]dd/M");
    dateAxis.renderer.grid.template.location = 0;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.max = (config.minimumNeeded + estimationDays * perDay_required) * 1.1;
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.grid.template.strokeDasharray = "1,0";
    valueAxis.renderer.labels.template.fill = am4core.color("#67B7DC");
    valueAxis.fontWeight = "400";

    var series = chart.series.push(new am4charts.LineSeries());
    series.name = "Nombre moyen de soutiens nécessaires";
    series.dataFields.dateX = "date";
    series.dataFields.valueY = "cumul_theorique";
    series.tooltip.disabled = true
    series.fill = am4core.color("#e59165");
    series.stroke = series.fill;
    //series.strokeOpacity = 0.6;
    series.strokeWidth = 4;
    //series.strokeDasharray = 4;
    series.data = config.dataSerie_minimum;

    var series2 = chart.series.push(new am4charts.LineSeries());
    series2.id = "g2";
    series2.name = "Soutiens publiés";
    series2.dataFields.dateX = "date";
    series2.dataFields.valueY = "visits";
    series2.xAxis = dateAxis;
    series2.fill = am4core.color("#67B7DC");
    series2.stroke = series2.fill;
    series2.strokeWidth = 5;
    series2.strokeDasharray = 0;
    series2.data = config.dataSerie_visits;
    series2.tooltip.pointerOrientation = "vertical"
    // construction tooltip html sur courbe "soutiens affichés par l'intérieur"
    series2.tooltipHTML = `
        <div class="text-left mt-2"><strong>{valueY.value}</strong> soutiens publiés au <strong>{dateX}</strong></div>
        <div class="text-left"><strong>{cumul_theorique}</strong> soutiens moyens nécessaires</div>
        <div class="my-2" style="border-top: 1px solid white"></div>
        <div class="text-left mb-2"><strong>{difference}</strong> d'écart avec moyenne requise</div>`

    var series3 = chart.series.push(new am4charts.LineSeries());
    series3.name = "Nombre officiel de soutiens (Conseil Constitutionnel)";
    series3.dataFields.dateX = "date";
    series3.dataFields.valueY = "visits";
    series3.xAxis = dateAxis;
    series3.fill = am4core.color("#808000");
    series3.stroke = series3.fill;
    series3.strokeWidth = 0;
    series3.data = config.dataSerie_officiel;
    series3.tooltip.pointerOrientation = "vertical"

    // Serie d'estimation de l'evolution
    var series4 = chart.series.push(new am4charts.LineSeries());
    series4.name = "Estimation";
    series4.dataFields.dateX = "date";
    series4.dataFields.valueY = "estimation";
    series4.xAxis = dateAxis;
    series4.fill = am4core.color("#67B7DC");
    series4.stroke = series2.fill;
    series4.strokeWidth = 0;
    series4.tooltip.pointerOrientation = "vertical"

    var bullet4 = series4.bullets.push(new am4charts.CircleBullet())
    bullet4.circle.fill = series4.fill
    bullet4.stroke = series4.fill
    bullet4.tooltipText = "[bold]{estimation}[/] estimés au {dateX}"
    bullet4.circle.radius = 3
    if (estimationDays > 31) {
        bullet4.circle.radius = 1
    } else if (estimationDays >= 15) {
        bullet4.circle.radius = 2
    }

    // Ajout bullet sur les soutiens necessaires (aujourd'hui seulement)
    var bullet = series.bullets.push(new am4charts.CircleBullet());
    bullet.circle.radius = 6;
    bullet.circle.strokeWidth = 0;

    var labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = "{cumul_theorique}";
    labelBullet.label.fill = am4core.color("#e59165");
    labelBullet.label.fontSize = "14px";
    labelBullet.label.fontWeight = "bold";
    labelBullet.label.dy = -25;

    // Ajout bullet sur les soutiens (aujourd'hui seulement)
    var bullet2 = series2.bullets.push(new am4charts.CircleBullet());
    bullet2.circle.radius = 6;
    bullet2.circle.strokeWidth = 0;

    var labelBullet2 = series2.bullets.push(new am4charts.LabelBullet());
    labelBullet2.label.text = "{visits}";
    labelBullet2.label.fill = series2.fill;
    labelBullet2.label.fontSize = "14px";
    labelBullet2.label.fontWeight = "bold";
    labelBullet2.label.dy = 25;

    // Add simple bullet
    var bullet3 = series3.bullets.push(new am4charts.CircleBullet());
    bullet3.circle.stroke = series3.stroke;
    bullet3.circle.radius = 4;
    bullet3.circle.strokeWidth = 3;
    bullet3.tooltipText = "Nombre officiel de soutiens : {valueY}";
    series3.tooltip.pointerOrientation = "vertical";
    series3.tooltip.dy = -20;

    var labelBullet3 = series3.bullets.push(new am4charts.LabelBullet());
    labelBullet3.label.text = "{visits}";
    labelBullet3.label.fill = series3.fill;
    labelBullet3.label.fontSize = "14px";
    labelBullet3.label.fontWeight = "bold";
    labelBullet3.label.dy = -20;

    // Create ranges
    var range = dateAxis.axisRanges.create();
    range.date = config.lastUpdate.clone().add(12, 'hours').toDate()
    range.endDate = config.dateEnd.toDate()
    range.axisFill.fill = am4core.color("#ddd")
    range.axisFill.fillOpacity = 0.3

    // Curseur (sans zoom)
    chart.cursor = new am4charts.XYCursor()
    chart.cursor.xAxis = dateAxis
    chart.cursor.behavior = 'none'

    // Pour le moment, on enleve l scroll chart (pas assez de jours pour le justifier)
    //var scrollbarX = new am4charts.XYChartScrollbar();
    //scrollbarX.series.push(series2);
    //chart.scrollbarX = scrollbarX;
    //scrollbarX.scrollbarChart.events.on("ready", function (ev) {
    //    ev.target.fontSize = "14px";
    //    ev.target.fillOpacity = 0;
    //});

    // On affiche les labels sur la date du dernier compte
    function showLastCumul(fillOpacity, target) {
        return (target.dataItem.index === config.lastUpdateIndex) ? 1 : 0
    }
    function showLastSoutiens(fillOpacity, target) {
        return (target.dataItem.index === series2.data.length - 1) ? 1 : 0
    }
    bullet.adapter.add("fillOpacity", showLastCumul)
    labelBullet.label.adapter.add("fillOpacity", showLastCumul)
    bullet2.adapter.add("fillOpacity", showLastSoutiens)
    labelBullet2.label.adapter.add("fillOpacity", showLastSoutiens)

    // la grille presque invisible permet d'alléger le graphique déjà bien chargé
    // ça permet aussi de mieux voir les prolongements hachurés au survol avec XYCursor
    valueAxis.renderer.grid.template.strokeOpacity = 0.3;
    dateAxis.renderer.grid.template.strokeOpacity = 0.08;

    // Configuration de la legende
    chart.legend = new am4charts.Legend();
    series.legendSettings.labelText = "[bold {color}]{name}[/]"
    series2.legendSettings.labelText = "[bold {color}]{name}[/]"
    series3.legendSettings.labelText = "[bold {color}]{name}[/]"
    series4.legendSettings.labelText = "[bold {color}]{name}[/]"
}

function chartTendance(config) {
    var data = [];
    config.chartData.forEach((obj, index) => {
        var dataObj = {};
        dataObj["date"] = obj["date"];
        dataObj["différence avec les soutiens nécessaires"] = obj["visits"] - obj["cumul_theorique"];
        dataObj["soutiens affichés journaliers"] = obj["visits"]
        if (index > 0 && config.chartData[index - 1].hasOwnProperty('visits')) {
            dataObj["soutiens affichés journaliers"] -= config.chartData[index - 1]["visits"];
        }
        if (obj.hasOwnProperty('visits')) {
            data.push(dataObj)
        }
    });

    var dataSerie_evolution = data.filter(obj => obj["soutiens affichés journaliers"] && obj["soutiens affichés journaliers"] !== 0);

    // Themes begin
    am4core.useTheme(am4themes_material);
    am4core.options.autoSetClassName = true;

    // Create chart instance
    var chart = am4core.create("chartTendance-div", am4charts.XYChart);

    // menu chart
    chart.exporting.menu = new am4core.ExportMenu();
    chart.exporting.menu.items = config.menuOptions;

    // formatage
    chart.language.locale = am4lang_fr_FR;
    chart.language.locale["_date_day"] = "dd";
    chart.language.locale["_decimalSeparator"] = ",";
    chart.language.locale["_thousandSeparator"] = " ";

    chart.dateFormatter.dateFormat = "dd/MM/yyyy";

    /* Create axes */
    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.dateFormats.setKey("day", "dd/MM");
    dateAxis.periodChangeDateFormats.setKey("day", "[bold]MM");
    dateAxis.renderer.grid.template.location = 0;

    /* Create value axis */
    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.max = 300000
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.grid.template.strokeDasharray = "2,3";
    valueAxis.renderer.labels.template.fill = am4core.color("#67B7DC");
    valueAxis.fontWeight = "400";

    /* Create series */
    var columnSeries = chart.series.push(new am4charts.ColumnSeries());
    columnSeries.name = "différence avec les soutiens nécessaires";
    columnSeries.dataFields.valueY = "différence avec les soutiens nécessaires";
    columnSeries.dataFields.dateX = "date";
    columnSeries.fill = am4core.color("#61738B");
    columnSeries.fillOpacity = 0.8;
    columnSeries.strokeWidth = 0;
    columnSeries.data = data;
    columnSeries.tooltipText =
        "[#fff font-size: 15px]différence\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]";
    columnSeries.tooltip.label.textAlign = "middle";

    columnSeries.columns.template.adapter.add("fill", function (fill, target) {
        if (target.dataItem && (target.dataItem.valueY < 0)) {
            return am4core.color("#a55");
        } else {
            return fill;
        }
    });

    var columnBullet = columnSeries.bullets.push(new am4charts.LabelBullet);
    columnBullet.label.text = "{différence avec les soutiens nécessaires}";
    columnBullet.label.fontSize = 14;
    columnBullet.label.fontWeight = "bold";
    columnBullet.label.rotation = 270;
    columnBullet.label.truncate = false;
    columnBullet.label.hideOversized = false;

    var lastColumn = columnSeries.data.length - 1;
    var columnMax = Math.max(...columnSeries.data.map(obj => obj["différence avec les soutiens nécessaires"]).sort());
    columnBullet.label.adapter.add("fillOpacity", (fillOpacity, target) => {
        var index = target.dataItem.index;
        return (index === 0) ? 1 : (target.dataItem.valueY === columnMax) ? 1 : (index === lastColumn) ? 1 : 0;
    });

    var lineSeries = chart.series.push(new am4charts.LineSeries());
    lineSeries.id = "g2";
    lineSeries.name = "soutiens affichés journaliers";
    lineSeries.dataFields.dateX = "date";
    lineSeries.dataFields.valueY = "soutiens affichés journaliers";
    lineSeries.xAxis = dateAxis;
    lineSeries.fill = am4core.color("#67B7DC");
    lineSeries.stroke = lineSeries.fill;
    lineSeries.strokeWidth = 3;
    lineSeries.data = dataSerie_evolution;

    // Add simple bullet
    var bullet = lineSeries.bullets.push(new am4charts.CircleBullet());
    bullet.circle.stroke = am4core.color("#fff");
    bullet.circle.radius = 1;
    bullet.circle.strokeWidth = 1;
    bullet.tooltipText = "[#fff font-size: 15px]{name}\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]";

    var labelBullet = lineSeries.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = "{soutiens affichés journaliers}";
    labelBullet.label.fill = lineSeries.stroke;
    labelBullet.label.fontSize = 14;
    labelBullet.label.fontWeight = "bold";
    labelBullet.label.dy = -10;
    labelBullet.label.dx = -20;

    var last = lineSeries.data.length - 1;
    labelBullet.label.adapter.add("fillOpacity", (fillOpacity, target) => {
        var index = target.dataItem.index;
        return (index === 0) ? 1 : (index === last) ? 1 : 0;
    });
    labelBullet.label.adapter.add("fill", function (fill, target) {
        var values = target.dataItem.dataContext;
        return (target.dataItem && values["soutiens affichés journaliers"] < values["différence avec les soutiens nécessaires"]) ?
            am4core.color("#EBF9FF") : fill;
    });


    // ligne tendance (regression)
    var regseries = chart.series.push(new am4charts.LineSeries());
    regseries.dataFields.valueY = "soutiens affichés journaliers";
    regseries.dataFields.dateX = "date";
    regseries.strokeWidth = 2;
    regseries.stroke = am4core.color("#FF5500");
    regseries.fill = regseries.stroke;
    regseries.name = "Tendance";
    regseries.data = data;
    var reg = regseries.plugins.push(new am4plugins_regression.Regression());

    // Add simple bullet
    var regBullet = regseries.bullets.push(new am4charts.CircleBullet());
    regBullet.circle.stroke = regseries.stroke;
    regBullet.circle.radius = 1;
    regBullet.circle.strokeWidth = 0;
    regBullet.tooltipText = "{valueY}";
    regseries.tooltip.background.cornerRadius = 20;
    regseries.tooltip.background.strokeOpacity = 0;
    regseries.tooltip.pointerOrientation = "vertical";
    regseries.tooltip.dy = -10;
    regseries.tooltip.background.fill = regseries.stroke;

    regseries.events.on("ready", function () {
        var regLabel = regseries.createChild(am4core.Label);
        regLabel.fontSize = 14;
        regLabel.fontWeight = "normal";
        regLabel.fill = am4core.color("#FF5500");
        regLabel.strokeWidth = 0;
        regLabel.fillOpacity = 1;
        regLabel.padding(0, 0, 2, 0);

        regLabel.path = regseries.segments.getIndex(0).strokeSprite.path;
        regLabel.text = "Tendance";

        regseries.segments.getIndex(0).strokeSprite.events.on("propertychanged", function (event) {
            if (event.property == "path") {
                regLabel.path = regseries.segments.getIndex(0).strokeSprite.path;
            }
        });

        var regClose = reg.result.points[reg.result.points.length - 1][1];
        var html;
        var estimation = Math.round(config.days_remaining * regClose) + config.latestCount;
        var html = '<i>Prévision sur tendance (linéaire) :<br/><b>'
            + chart.numberFormatter.format(estimation)
            + " </b>d'&eacute;cart avec les soutiens necessaires le <b>12/03/2020**</b></i>."
        document.getElementById('estimation-output').innerHTML = html;

    }, 1000);

    chart.cursor = new am4charts.XYCursor();
    chart.cursor.xAxis = dateAxis;

    // Pour le moment, on enleve l scroll chart (pas assez de jours pour le justifier)
    //var scrollbarX = new am4charts.XYChartScrollbar();
    //scrollbarX.series.push(lineSeries);
    //chart.scrollbarX = scrollbarX;
    //scrollbarX.scrollbarChart.events.on("ready", function (ev) {
    //    ev.target.fontSize = "14px";
    //    ev.target.fillOpacity = 0;
    //});

    chart.legend = new am4charts.Legend();

    columnSeries.legendSettings.labelText = "[bold {color}]{name}[/]";
    lineSeries.legendSettings.labelText = "[bold {color}]{name}[/]"; // {valueY.sum}[/]
    regseries.legendSettings.labelText = "[bold {color}]{name}[/]";

    var markerTemplate = chart.legend.markers.template;
    markerTemplate.width = 20;
    markerTemplate.height = 10;

    chart.data = data;
}

$(() => {
    $('#header').load('navigation.html');
    document.getElementById('main-container').style.display = "block";

    document.getElementById('guide-btn').onclick = function (e) {
        var offsetHeigth = document.getElementById('chartTendance-div').offsetHeight;
        var intro = introJs();
        var elementTendance = {
            element: `#estimation-output`,
            intro: `<p>Prévision de tendance sur le nombre final des soutiens qui pourrait être affiché à la date butoir du 12/03/2010</p>`,
            position: `top`
        };
        var options = [{
                intro: `<h4>Bienvenue dans la visite guidée.</h4><p>Vous pouvez interrompre la visite à tout moment
                        et y revenir quand vous voulez en cliquant sur "Visite guidée"</p>`
            },
            {
                element: `#soutiens_valides`,
                intro: `Nombre de soutiens publiés sur le site du ministère de l’Intérieur<br/>
                <a rel="nofollow" href="https://www.referendum.interieur.gouv.fr/consultation_publique/8" target="_blank"
                    title="Consulter les listes de soutiens">referendum.interieur.gouv.fr</a>`,
                position: 'bottom'
            },
            {
                element: `#perDay_required`,
                intro: `Nombre de soutiens que nous devrions avoir aujoud'hui pour &ecirc;tre en bonne voie pour atteindre l'objectif
                    de 4,7 millions de soutiens, soit 10% du corps électoral`,
                position: 'bottom'
            },
            {
                element: `#average_currentWeek`,
                intro: `Moyenne journalière réalisée au cours des 7 derniers jours pour
                    les soutiens affichés par le site du ministère de l’Intérieur`,
                position: 'bottom'
            },
            {
                element: `#output`,
                intro: `<p>Estimation d'une date de fin "virtuelle" calculée à partir des soutiens déjà validés
                    et du nombre de jours restants qu'il faudrait pour obtenir le nombre de soutiens nécessaires.</p>
                    <p>Si la moyenne des soutiens validés est trop basse, le "seuil minimum requis" d’environ 4,7 millions de soutiens,
                    soit 10% du corps électoral pourrait ne pas être atteint pour le 12/03/2019 date butoir.</p>
                    <p><a rel="nofollow" href="https://www.referendum.interieur.gouv.fr/" target="_blank" title="referendum.interieur.gouv.fr">En savoir plus...</a></p>`,
                position: 'top'
            },
            {
                element: `#gaugediv`,
                intro: `<p>Pour atteindre l'objectif d’environ 4,7 millions de soutiens,</p>
                <p>les "soutiens publiés" <b>doivent dépasser</b> les "soutiens nécessaires".</p>`,
                position: 'bottom'
            },
            {
                element: `#selectCharts`,
                intro: `Permet de passer d'un graphique à l'autre`,
                position: 'top'
            }
        ];
        if (offsetHeigth > 0)
            options.splice(4, 1, elementTendance);

        var optionsTendance =
            intro.setOptions({
                tooltipPosition: 'top',
                nextLabel: 'Suivant',
                prevLabel: 'Retour',
                skipLabel: 'Sortir',
                doneLabel: 'Terminer',
                showProgress: true,
                showStepNumbers: true,
                steps: options
            });
        intro.start();
    }
});
